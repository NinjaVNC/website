function feedback(msg) {
        $("#msg").html(msg);
    }

    function signup() {

        $('#submit').attr('disabled', 'disabled');
        var request = $.ajax({
            url: "https://9w1qkejic0.execute-api.eu-west-1.amazonaws.com/prod/ossnotify",
            type: "POST",
            method : "POST",
            data: JSON.stringify({
                "email":$("#email").val()
            }),
            dataType: "json",
            contentType: "application/json"
        });

        request.done(function( msg ) {
            $('#submit').removeAttr('disabled');
            try {
                var msgObj = JSON.parse(msg);
                if(msgObj.result == "ok") {

                    feedback( "Thanks for signing up. We will get back to you when we have spiders in stock");
                }
                else {
                    feedback( "Failed to sign you up due to the following server error: " + JSON.stringify(msgObj.message));
                }
            }
            catch(e) {
                feedback( "Failed to parse response " + msg);
            }
        });

        request.fail(function( jqXHR, textStatus ) {
            $('#submit').removeAttr('disabled');
            feedback( "Failed to sign you up due to the following client error: " + JSON.stringify(jqXHR));
        });
    }

    function check() {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

        if (!pattern.test($("#email").val())) {
            $('#submit').attr('disabled', 'disabled');
        } else {
            $('#submit').removeAttr('disabled');
        }
    }

    $('#email').keyup(function() {
        check();
    });

    $('#email').blur(function() {
        check();
    });

