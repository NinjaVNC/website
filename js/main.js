$(document).ready(function(){
		  var touch = $(".touch_menu");
		  var menu = $(".nav");
		  $(touch).on("click", function(e){
		  	e.preventDefault();
			menu.slideToggle();
		  });
		  $(window).resize(function(){
		  	var wid = $(window).width();
			if (wid > 750 && menu.is(':hidden')) {
				menu.removeAttr('style');
			}
		  });
		  
	  });
		
		
	/* Scroll to ID */	
		
		$(document).on('click', 'a[href^="#"]', function(e) {
    // target element id
    var id = $(this).attr('href');
    
    // target element
    var $id = $(id);
    if ($id.length === 0) {
        return;
    }
    
    // prevent standard hash navigation (avoid blinking in IE)
    e.preventDefault();
    
    // top position relative to the document
    var pos = $(id).offset().top;
    // animated top scrolling
	var scrollTime = pos / 1.73;
	console.log(pos);
	console.log(scrollTime);
    $('body, html').animate({'scrollTop': pos},{duration: scrollTime});
});
		
		/* Scroll to ID */
		$(window).scroll(function(){
		
			var wScroll = $(this).scrollTop();
			
			$('.card').css({
				'transform' : 'translate('+ wScroll /8 +'%, '+ wScroll /8 +'%)' 
			});
			
			$('.usb').css({
				'transform' : 'translate('+ wScroll /8 +'%, '+ wScroll /8 +'%)' 
			});
			
			$('.hdmi').css({
				'transform' : 'translate(0px, '+ wScroll /25 +'%)'  
			});
			
			if(wScroll > $('.units_wrapper').offset().top- ($(window).height() / 1.5)) {
			
			$('.units_wrapper figure').each(function(i){
				
				setTimeout(function(){
				$('.units').eq(i).addClass('is-showing');
				}, 150 * (i+1));
				
			});
				
			};
				
			if(wScroll > $('.protocol_unit').offset().top- ($(window).height() / 1.5)) {
				
				var offset = Math.min(0, wScroll - $('.protocol_unit').offset().top + $(window).height() - 250);
			
				$('.transRight').css({'transform': 'translate('+ -offset +'px, 0px)'});
				
			}
			
			if(wScroll > $('.tor_unit').offset().top- ($(window).height() / 1.5)) {
				
				var offset = Math.min(0, wScroll - $('.tor_unit').offset().top + $(window).height() - 250);
			
				$('.transLeft').css({'transform': 'translate('+ offset +'px, 0px)'});
				
			}
			
			if(wScroll > $('.simple').offset().top- ($(window).height() / 1.5)) {
				
				var offset = Math.min(0, wScroll - $('.simple').offset().top + $(window).height() - 350);
			
				$('.open').css({'transform': 'translate('+ -offset +'px, 0px)'});
				$('.simple').css({'transform': 'translate('+ offset +'px, 0px)'});
				
			}
			
			if(wScroll > $('.private').offset().top- ($(window).height() / 1.5)) {
				
				var offset = Math.min(0, wScroll - $('.private').offset().top + $(window).height() - 350);
			
				$('.secure').css({'transform': 'translate('+ -offset +'px, 0px)'});
				$('.private').css({'transform': 'translate('+ offset +'px, 0px)'});
				console.log(wScroll);
			}
			/*----Footer----*/
		/*	if(wScroll > $('.buy_box').offset().top - 360) {
				$('.footer').show();
			}
			else {
				$('.footer').hide();
			}  */
			if(wScroll + $(window).height() >= $('body').height() - 120) {
				$('.footer').show();
			}
			else {
				$('.footer').hide();
			}
			
		});